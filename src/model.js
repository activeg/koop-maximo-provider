/*
  model.js

  This file is required. It must export a class with at least one public function called `getData`

  Documentation: http://koopjs.github.io/docs/usage/provider
*/
const config = require('config')
const reproject = require('reproject');
const proj4 = require('proj4')
var epsg = require('epsg');
var Maximo = require('ibm-maximo-api');

function Model(koop) { }

// Public function to return data from the
// Return: GeoJSON FeatureCollection
//
// Config parameters (config/default.json)
// req.
//
// URL path parameters:
// req.params.host (if index.js:hosts true)
// req.params.id  (if index.js:disableIdParam false)
// req.params.layer
// req.params.method
Model.prototype.getData = function (req, callback) {
  const object = config.maximo.object;
  const options = config.maximo.options;
  const select = config.maximo.select;
  const where = config.maximo.where;
  const woSR = config.maximo.spatialReference;

  if (!req.session)
    req.session = {};

  var maximo = new Maximo(options);
  maximo.resourceobject(object.toUpperCase())
    .select(select)
    .where(where)
    .fetch()
    .then(function (data) {
      jsondata = data.thisResourceSet();
      var geojson = translate(jsondata);

      const fromSR = proj4.defs(woSR);
      try {
        geojson = reproject.toWgs84(geojson, fromSR, epsg);
      } catch (err) {
        console.log(err);
      }

      callback(null, geojson)
    })
}

function translate(input) {
  let records = input.map(formatFeature);
  return {
    type: 'FeatureCollection',
    metadata: {
      name: "Work Orders",
      description: "Open Maximo Work Orders",
      idField: "workorderid"
    },
    features: records
  }
}

function formatFeature(inputFeature) {
  let a = Object.assign({}, inputFeature);
  let attr = {};

  for (var i = 0; i < config.maximo.select.length; i++) {
    let p = config.maximo.select[i];
    attr[p] = a[p];
  }

  let x = a[config.maximo.xfield];
  let y = a[config.maximo.yfield];
  let z = config.maximo.zfield ? a[config.maximo.zfield] : 1;
  // Most of what we need to do here is extract the longitude and latitude
  const feature = {
    type: 'Feature',
    properties: attr,
    geometry: {
      type: 'Point',
      coordinates: [x, y, z]
    }
  }
  // But we also want to translate a few of the date fields so they are easier to use downstream
  /*
  const dateFields = ['expires', 'serviceDate', 'time']
  dateFields.forEach(field => {
    feature.properties[field] = new Date(feature.properties[field]).toISOString()
  })
  */
  return feature;
}

module.exports = Model
