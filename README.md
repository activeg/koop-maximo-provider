# Koop Provider for Maximo

## Getting started
* Open `config/default.json` to set configuration
    * Set port number to use for application
    * Define Maximo connection information in maximo.options
    * Define Maximo Object Structure, Where Clause and Fields to include (object, where, select)
    * Define XYZ field names from Maximo Work Order table (xfield, yfield, zfield)
* Open `src/index.js` and change `provider.name` to a unique name
* Install dependencies `npm install`
* Run a local dev server `npm start`
* This provider can also be added to an existing Koop Application as a Plugin. The configuration of that depends on how the Koop Application is configured. See [Koop documentation](https://koopjs.github.io/docs/development/registration) for more details

This is a Koop provider that outputs work order from Maximo into a Feature Service.

Full documentation on how to use a Koop Provider is provided [here](https://koopjs.github.io/docs/usage/provider).

## Koop provider file structure

| File | | Description |
| --- | --- | --- |
| `src/index.js` | Mandatory | Configures provider for usage by Koop |
| `src/model.js` | Mandatory | Translates remote API to GeoJSON |
| `src/server.js` | Mandatory | Launches the Koop Server, necessary if launching independently |
| `config/default.json` | Mandatory | used for advanced configuration, usually API keys. |
| `src/routes.js` | Optional | Specifies additional routes to be handled by this provider |
| `src/controller.js` | Optional | Handles additional routes specified in `routes.js` |


## Test it out
Run server:
- `npm install`
- `npm start`

Example API Query:
- `curl localhost:8088/mxwo/FeatureServer/0/query?returnCountOnly=true`

